module zabbix.com

go 1.17

require (
	git.zabbix.com/ap/plugin-support v1.2.2-0.20230123113531-5ae9ac97afce
	github.com/BurntSushi/locker v0.0.0-20171006230638-a6e239ea1c69
	github.com/Microsoft/go-winio v0.6.0
	github.com/chromedp/cdproto v0.0.0-20210104223854-2cc87dae3ee3
	github.com/chromedp/chromedp v0.6.0
	github.com/dustin/gomemcached v0.0.0-20160817010731-a2284a01c143
	github.com/eclipse/paho.mqtt.golang v1.2.0
	github.com/fsnotify/fsnotify v1.4.9
	github.com/go-ldap/ldap v3.0.3+incompatible
	github.com/go-ole/go-ole v1.2.4
	github.com/go-sql-driver/mysql v1.5.0
	github.com/goburrow/modbus v0.1.0
	github.com/godbus/dbus v4.1.0+incompatible
	github.com/godror/godror v0.20.1
	github.com/mattn/go-sqlite3 v1.14.8
	github.com/mediocregopher/radix/v3 v3.5.0
	github.com/memcachier/mc/v3 v3.0.1
	github.com/miekg/dns v1.1.43
	github.com/omeid/go-yarn v0.0.1
	github.com/sokurenko/go-netstat v1.0.0
	golang.org/x/sys v0.0.0-20220722155257-8c9f86f7a55f
)

require (
	github.com/chromedp/sysutil v1.0.0 // indirect
	github.com/go-logfmt/logfmt v0.5.0 // indirect
	github.com/goburrow/serial v0.1.0 // indirect
	github.com/gobwas/httphead v0.1.0 // indirect
	github.com/gobwas/pool v0.2.1 // indirect
	github.com/gobwas/ws v1.0.4 // indirect
	github.com/josharian/intern v1.0.0 // indirect
	github.com/mailru/easyjson v0.7.6 // indirect
	golang.org/x/mod v0.6.0-dev.0.20220419223038-86c51ed26bb4 // indirect
	golang.org/x/net v0.0.0-20220722155237-a158d28d115b // indirect
	golang.org/x/tools v0.1.12 // indirect
	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1 // indirect
	gopkg.in/asn1-ber.v1 v1.0.0-20181015200546-f715ec2f112d // indirect
)
